// import { NavLink } from 'react-router-dom';

// function Nav() {
//   return (
//     <nav className="navbar navbar-expand-lg navbar-dark bg-success">
//       <div className="container-fluid">
//         <NavLink className="navbar-brand" to="/">CarCar</NavLink>
//         <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//           <span className="navbar-toggler-icon"></span>
//         </button>
//         <div className="collapse navbar-collapse" id="navbarSupportedContent">
//           <ul className="navbar-nav me-auto mb-2 mb-lg-0">
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/manufacturer">Manufacturer List</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/manufacturer/new">Create Manufacturer</NavLink>
//             </li>
//             <li>
//               <NavLink className="nav-link" to="/models">Vehicle Models List</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/models/new">Create Vehicle Model</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/automobiles/new">Create Automobile</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/automobiles">Automobiles List</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/technicianform/new">Enter a technician</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/serviceappointmentform/new">Enter a service appointment</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/appointmentlist">Appointment List</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="/servicehistory">Service History</NavLink>
//             </li>
//             <li>
//               <NavLink className="nav-link" to="salesrecords/">Sales Record List</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="salesreps/">Sales Rep List</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="salescustomers/">Create Sales Customer</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="salesreps/new">Create Sales Rep</NavLink>
//             </li>
//             <li className="nav-item">
//               <NavLink className="nav-link" to="salesrecords/new">Create Sales Record</NavLink>
//             </li>
//           </ul>
//         </div>
//       </div>
//     </nav>
//   )
// }

// export default Nav;

import { NavLink } from 'react-router-dom';
import './css/nav.css';

function Nav() {
  return (
    <nav className="navbar">
      <div>
        <NavLink className="name-link" to="/">Exclusive Auto Haus</NavLink>
        <button 
          className="navbar-button"
          type="button" 
          data-bs-toggle="collapse" 
          data-bs-target="#navbarSupportedContent" 
          aria-label="Toggle navigation">
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-placement">
            <li>Manufacturer
              <ul>
                <li>
                  <NavLink to="/manufacturer">Manufacturer List</NavLink>
                </li>
                <li>
                  <NavLink to="/manufacturer/new">Create Manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li>Models
              <ul>
                <li>
                  <NavLink to="/models">Vehicle Models List</NavLink>
                </li>
                <li>
                  <NavLink to="/models/new">Create Vehicle Model</NavLink>
                </li>
              </ul>
            </li>
            <li>Automobiles
              <ul>
                <li>
                  <NavLink to="/automobiles/new">Create Automobile</NavLink>
                </li>
                <li>
                  <NavLink to="/automobiles">Automobiles List</NavLink>
                </li>
              </ul>
            </li>
            <li>Services
              <ul>
                <li>
                  <NavLink to="/technicianform/new">Enter a technician</NavLink>
                </li>
                <li>
                  <NavLink to="/serviceappointmentform/new">Enter a service appointment</NavLink>
                </li>
                <li>
                  <NavLink to="/appointmentlist">Appointment List</NavLink>
                </li>
                <li>
                  <NavLink to="/servicehistory">Service History</NavLink>
                </li>
              </ul>
            </li>
            <li>Sales
              <ul>
                <li>
                  <NavLink to="salesrecords/">Sales Record List</NavLink>
                </li>
                <li>
                  <NavLink to="salesreps/">Sales Rep List</NavLink>
                </li>
                <li>
                  <NavLink to="salescustomers/">Create Sales Customer</NavLink>
                </li>
                <li>
                  <NavLink to="salesreps/new">Create Sales Rep</NavLink>
                </li>
                <li>
                  <NavLink to="salesrecords/new">Create Sales Record</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
